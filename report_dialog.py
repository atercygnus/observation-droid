import pandas as pd

from telethon import TelegramClient
import psycopg2
import asyncstdlib as a
import nltk

from utils import get_config

def calc_daily_stat(conn):

    msg_query = '''
	with t_msg_cnt as (
	    select
		author_id,
		count(distinct id) as msg_count,
		row_number() over(order by count(distinct id) desc) as rnk
	    from messages
	    where date_trunc('day', message_timestamp) = now()::date - interval '1 day' 
	    group by author_id
	    order by msg_count desc
	)
	(select first_name || ' ' || coalesce(last_name, '') as author, msg_count from t_msg_cnt left join users on users.id = t_msg_cnt.author_id order by msg_count desc limit 10)
	union all
	(select 'Все другие' as author_name, coalesce(sum(msg_count), 0) from t_msg_cnt where rnk > 10);
    '''
    
    cur = conn.cursor()
    cur.execute(msg_query)
    msgcount = cur.fetchall()

    return msgcount


async def extract_and_save_dialog(tg_client, conf, msgcount):

    await tg_client.send_message(conf['chat_id'], f"```{msgcount.set_index('Сообщений за сутки').to_markdown(tablefmt='plain')}```")


if __name__ == "__main__":

    conf = get_config()

    client = TelegramClient(conf['bot_name'], conf['api_id'], conf['api_hash'])

    conn = psycopg2.connect(
        host=conf['pghost'], 
        port=conf['pgport'], 
        database=conf['database'], 
        user=conf['user'], 
        password=conf['password']
    )

    msgcount = pd.DataFrame(calc_daily_stat(conn), columns=['Сообщений за сутки', ''])

    with client:
        dialog = client.loop.run_until_complete(extract_and_save_dialog(client, conf, msgcount))

