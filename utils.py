import os
import yaml

def get_config():
    
    with open(os.path.join(os.path.dirname(__file__),'conf')) as f:
        conf = yaml.load(f, Loader=yaml.FullLoader)

    return conf
