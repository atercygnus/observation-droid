from datetime import datetime
import pandas as pd

import psycopg2
import nltk

from utils import get_config

import pymorphy2

def get_max_token(cur):

    cur.execute('select max(day) as max_day from tokens;')

    res = cur.fetchone()[0];

    return res if res is not None else datetime(2000, 1, 1)


def get_tokens(cur):

    tokens_query = '''
	select day, channel_id, word, count(*) as word_count from( 
	    select 
		date_trunc('day', message_timestamp) as day, 
		channel_id,
		unnest(string_to_array(lower(regexp_replace(text, '[^-а-яА-Я ёЁ]', '', 'g')), ' ')) as word 
	    from messages 
	    where message_timestamp > '{}'
	    order by message_timestamp desc
	) as t
        where 1=1
          and length(word) > 1
          and word not in ('', 'нету' ,'вообще' ,'тебе', 'наши','просто', 'это', 'и', 'в', 'во', 'не', 'что', 'он', 'на', 'я', 'с', 'со', 'как', 'а', 'то', 'все', 'она', 'так', 'его', 'но', 'да', 'ты', 'к', 'у', 'же', 'вы', 'за', 'бы', 'по', 'только', 'ее', 'мне', 'было', 'вот', 'от', 'меня', 'еще', 'нет', 'о', 'из', 'ему', 'теперь', 'когда', 'даже', 'ну', 'вдруг', 'ли', 'если', 'уже', 'или', 'ни', 'быть', 'был', 'него', 'до', 'вас', 'нибудь', 'опять', 'уж', 'вам', 'ведь', 'там', 'потом', 'себя', 'ничего', 'ей', 'может', 'они', 'тут', 'где', 'есть', 'надо', 'ней', 'для', 'мы', 'тебя', 'их', 'чем', 'была', 'сам', 'чтоб', 'без', 'будто', 'чего', 'раз', 'тоже', 'себе', 'под', 'будет', 'ж', 'тогда', 'кто', 'этот', 'того', 'потому', 'этого', 'какой', 'совсем', 'ним', 'здесь', 'этом', 'один', 'почти', 'мой', 'тем', 'чтобы', 'нее', 'сейчас', 'были', 'куда', 'зачем', 'всех', 'никогда', 'можно', 'при', 'наконец', 'два', 'об', 'другой', 'хоть', 'после', 'над', 'больше', 'тот', 'через', 'эти', 'нас', 'про', 'всего', 'них', 'какая', 'много', 'разве', 'три', 'эту', 'моя', 'впрочем', 'хорошо', 'свою', 'этой', 'перед', 'иногда', 'лучше', 'чуть', 'том', 'нельзя', 'такой', 'им', 'более', 'всегда', 'конечно', 'всю', 'между')
	group by 1,2,3;
    '''

    cur.execute(tokens_query.format(get_max_token(cur).strftime('%Y-%m-%d')))
    tokens = cur.fetchall()
 
    return pd.DataFrame(tokens, columns=['day', 'channel', 'word', 'count'])
 

def process_tokens(tokens):

    morph = pymorphy2.MorphAnalyzer()

    tokens.word = tokens.word.apply(lambda x: morph.parse(x)[0].normal_form)
    tokens = tokens.groupby(['day', 'channel', 'word']).sum().reset_index()

    return tokens

def store_tokens(tokens):

    if len(tokens) == 0:
        return

    cur.execute('BEGIN')
    args_str = ','.join(cur.mogrify("(%s,%s,%s,%s)", x).decode('utf8') for x in tokens.values)
    cur.execute(f"INSERT INTO tokens VALUES {args_str} ON CONFLICT DO NOTHING")
    cur.execute('COMMIT')

    print(f'{len(tokens)} tokens added')


if __name__ == '__main__':
    
    conf = get_config()

    conn = psycopg2.connect(
        host=conf['pghost'], 
        port=conf['pgport'], 
        database=conf['database'], 
        user=conf['user'], 
        password=conf['password']
    )

    cur = conn.cursor()

    tokens = get_tokens(cur)
    print('tokens retreived')
    tokens = process_tokens(tokens)
    print('tokens processed')
    store_tokens(tokens)
    print('tokens stored')

    print('Done!')
