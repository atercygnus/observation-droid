import pandas as pd

from telethon import TelegramClient
import psycopg2
import asyncstdlib as a
import nltk

from utils import get_config

def calc_daily_stat(conn):

    tfidf_query = '''
	with
	  t_idf as (
		select
		token,
		log((select count(distinct day) from tokens)/count(distinct day)::float) as idf
		from tokens
		group by token)
	select
	    tokens.day,
	    tokens.token,
	    count/words_in_day::float as tf,
	    idf,
	    count/words_in_day::float*idf as tf_idf
	from tokens
	left join (select day, count(distinct token) as words_in_day from tokens group by day) as t on tokens.day = t.day
	left join t_idf on tokens.token = t_idf.token
	where tokens.day = (select max(day) from tokens)
	order by count/words_in_day::float*idf desc
	limit 5;
    '''
    
    cur = conn.cursor()
    cur.execute(tfidf_query)
    tfidf = cur.fetchall()

    return tfidf


async def extract_and_save_dialog(tg_client, conf, tfidf):

    await tg_client.send_message(conf['chat_id'], f"```Слова дня: {' '.join(tfidf.word.values)}```")


if __name__ == "__main__":

    conf = get_config()

    client = TelegramClient(conf['bot_name'], conf['api_id'], conf['api_hash'])

    conn = psycopg2.connect(
        host=conf['pghost'], 
        port=conf['pgport'], 
        database=conf['database'], 
        user=conf['user'], 
        password=conf['password']
    )

    tfidf = pd.DataFrame(calc_daily_stat(conn), columns=['day', 'word', 'tf', 'idf', 'tfidf'])

    with client:
        dialog = client.loop.run_until_complete(extract_and_save_dialog(client, conf, tfidf))

