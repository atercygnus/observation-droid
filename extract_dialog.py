from telethon import TelegramClient
import psycopg2
from utils import get_config 
import asyncstdlib as a


def get_max_message_id(conn):

    cur = conn.cursor()
    cur.execute('select coalesce(max(id), 0) as max_id from messages;')
    row = cur.fetchone()

    return row[0]

def store_users(cur, users):

    if len(users) == 0:
        return

    cur.execute('BEGIN')
    args_str = ','.join(cur.mogrify("(%s,%s,%s,%s,%s,%s,%s,%s,%s)", x).decode('utf8') for x in users)
    cur.execute(f"INSERT INTO users VALUES {args_str} ON CONFLICT DO NOTHING")
    cur.execute('COMMIT')

    print(f'{len(users)} users added')
    users.clear() 


def store_messages(cur, messages):

    if len(messages) == 0:
        return

    cur.execute('BEGIN')
    args_str = ','.join(cur.mogrify("(%s,%s,%s,%s,%s,%s,%s,%s,%s)", x).decode('utf8') for x in messages)
    cur.execute(f"INSERT INTO messages VALUES {args_str}")
    cur.execute('COMMIT')

    print(f'{len(messages)} messages added')
    messages.clear() 


async def extract_and_save_dialog(tg_client, pg_conn, conf, stop_at):

    users = []
    messages = [] 

    cur = pg_conn.cursor()

    async for i, message in a.enumerate(tg_client.iter_messages(conf['chat_id'])):

        if message.sender is None:
            continue

        if (i > 0) and (i % 1000 == 0):
            store_users(cur, users)
            store_messages(cur, messages)

        if message.id <= stop_at:
            store_users(cur, users)
            store_messages(cur, messages)
            break

        fwd_user_id = None
        fwd_channel_id = None
        if message.fwd_from is not None:
            fwd_user_id = getattr(message.fwd_from.from_id, 'user_id', None)
            fwd_channel_id = getattr(message.fwd_from.from_id, 'channel_id', None)

        messages.append((
            message.id, 
            message.date, 
            message.sender.id, 
            message.peer_id.channel_id,
            message.reply_to.reply_to_msg_id if message.reply_to else None,
            message.photo is not None,
            fwd_user_id,
            fwd_channel_id,
            message.text
        ))

        users.append((
            message.sender.id,
            message.sender.first_name,
            message.sender.last_name,
            message.sender.username,
            message.sender.phone,
            message.sender.deleted,
            message.sender.verified,
            message.sender.scam,
            message.sender.bot

        ))

    store_users(cur, users)
    store_messages(cur, messages)


if __name__ == "__main__":

    conf = get_config()

    client = TelegramClient(conf['bot_name'], conf['api_id'], conf['api_hash'])

    conn = psycopg2.connect(
        host=conf['pghost'], 
        port=conf['pgport'], 
        database=conf['database'], 
        user=conf['user'], 
        password=conf['password']
    )

    with client:
        dialog = client.loop.run_until_complete(extract_and_save_dialog(client, conn, conf, get_max_message_id(conn)))

