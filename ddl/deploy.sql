drop table if exists public.messages;
drop table if exists public.users;
drop table if exists public.tokens;

create table public.messages(
    id bigint primary key,
    message_timestamp timestamp,
    author_id bigint,
    channel_id bigint,
    reply_to_msg bigint,
    has_photo boolean,
    fwd_from_user bigint,
    fwd_from_channel bigint,
    text text
);

create index messages_timestamp_ndx on messages (date_trunc('day', message_timestamp));

create table public.users(
    id bigint primary key,
    first_name text,
    last_name text,
    username text,
    phone text,
    is_deleted boolean,
    is_verified boolean,
    is_scam boolean,
    is_bot boolean
);

create table public.tokens(
	day date,
	channel_id bigint,
	token text,
	count integer,
    unique (day, channel_id, token)
);

